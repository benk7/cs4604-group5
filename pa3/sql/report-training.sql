WITH 
    max as (select t.trainingname as "Max Certification" 
                from training t 
                join certification c on c.trainingid=t.pktraining
                group by t.trainingname 
                order by count(c.trainingid) desc limit 1),
                
    min as (select t.trainingname as "Min Certification" 
                from training t 
                join certification c on c.trainingid=t.pktraining
                group by t.trainingname 
                order by count(c.trainingid) asc limit 1 )
select * from max, min;