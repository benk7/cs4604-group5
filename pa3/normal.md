In order for our relational model to be in 3NF it must first be in 1NF and then 2NF.

For a relational model to be in 1NF, each attribute name must be unique, each attribute value must be single, and each row must be unique. In looking at our relational model you can see that the name of each attribute is unique in each table. You can see that each attribute value is single (ie no composite values) and that each row is unique by looking at the load.sql file or by querying each table. Therefore, our model is in 1NF.

For a relational model to be in 2NF it must first be in 1NF. Then any non-key attribute must not be dependent on any proper subset of the key (ie all partial dependencies are moved to another table). This means that if you have no composite keys your database is in 2NF. In looking at our relational model you can see that we have no composite keys and therefore is in 2NF.

Finally, for a relational model to be in 3NF it must first be in 2NF. Then non-primary key attributes do not depend on other non-primary key attributes (ie no transitive dependencies). If you were to construct a minimal basis of our tables, you could see (by using the 3NF Synthesis Algorithm) that we do not need to create any extra relations and therefore there are no transitive dependencies. 
