# CS4604 Project - Scenario 1: Hospital

## Project Description

We want to design a database schema for a hospital.  The patient is admitted to a hospital with a medical condition. The hospital maintains patients’ information. Name, age, sex, DOB, and address are recorded. Hospital identifies each patient by a unique id and creates a patient admission record. Each admission record has an admission number, admission date, and discharge date information. Hospital assigns a doctor to treat a patient. Hospital stores doctor’s name, id, specialty and year of experience. Each doctor has multiple patients.  A patient is admitted to a ward. Ward is identified by ward number, name, and type (e.g., medical/surgical). Each ward contains multiple beds. Bed has number and type (e.g., side room bed/ open ward bed) as well. If a patient needs a surgery then the hospital schedules an operation. Operation number, date, time, patient id and doctors’ id are required to schedule an operation. We assume multiple doctors participated in an operation.

## Entity Relationship

The entities in this project are a *Patient*, *Doctor*, *Admission*, *Ward*, *Bed*, and an *Operation*. A Patient *is admitted to* a Ward. A Patient also *has an* Admission Record and *gets an* Operation. A Ward *contains* a Bed. An Admission Record is *assigned to* a Doctor and a Doctor *performs* an Operation.

Each Entity is uniquely defined by pkNameOfEntity.

![ER Diagram](diagrams/scenario1-erd.png "ER Diagram")

## Relational

The diagram below shows the entity-relationship diagram converted to a relational schema.

![Relational Diagram](diagrams/scenario1-relational.png "Relational Diagram")

The corresponding sql can be found [here](sql/install.sql).

## Build/Install/Run

### Docker Playground

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user.

#### Start Postgresql server

`docker run -d --name postgres -e POSTGRES_HOST_AUTH_METHOD=trust postgres -c 'log_min_duration_statement=0'`

#### Create the schema

```
curl https://code.vt.edu/benk7/cs4604-group5/blob/master/scenario1/sql/install.sql > install.sql
docker run -it --rm -v $(pwd)/install.sql:/install.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /install.sql
```

#### Load data

```
curl https://code.vt.edu/benk7/cs4604-group5/blob/master/scenario1/sql/load.sql> load.sql
docker run -it --rm -v $(pwd)/load.sql:/load.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /load.sql
```

#### Browse the database

`docker run -it --rm --link postgres:postgres postgres psql -a -h postgres -U postgres`

Use `\d` to see the list of available relations.
