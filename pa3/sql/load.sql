/* Sample data to be inserted at run time 
   Note we fudge the dates to provide meaningful examples in the interface.
*/

/* Rank */
insert into Rank (RankName) values ('Firefighter');
insert into Rank (RankName) values ('Lieutenant');
insert into Rank (RankName) values ('Captain');
insert into Rank (RankName) values ('Chief');

/* Training */
insert into Training (TrainingName, ExamNeeded) values ('Firefighter 1 Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Firefighter 2 Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Firefighter 3 Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Firefighter 4 Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Vehicle Rescue Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Emergency Vehicle Operator Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Pump Operator Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Fire Investigator Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Paramedic Training', 1);
insert into Training (TrainingName, ExamNeeded) values ('Hazardous Materials Training', 1);

/* Instructor */
insert into Instructor (Name) values ('Richard Quintin');
insert into Instructor (Name) values ('John Williams');

/* VehicleType */
insert into VehicleType (VehicleTypeName) values ('Engine');
insert into VehicleType (VehicleTypeName) values ('Ladder Truck');
insert into VehicleType (VehicleTypeName) values ('Utility Vehicle');

/* FireHouse */
insert into FireHouse (Line1, Line2, City, State, ZIP) values ('101 Main St.', '', 'Blacksburg', 'VA', 24060);

/* Employee */
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Louis', current_date, 'Davids', '1995-02-02', 4);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Austin', current_date, 'Spencer', '1998-06-06', 3);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Ben', current_date, 'Kernstine', '1997-12-01', 2);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Sami', current_date, 'Morency', '1998-02-17', 2);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Chris', current_date, 'Jones', '1990-04-08', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('David', current_date, 'Anderson', '1995-08-02', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Ellie', current_date, 'Barnes', '1992-01-01', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Farrah', current_date, 'Nelson', '1988-02-04', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Garrett', current_date, 'Miles', '1999-09-15', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Harrison', current_date, 'Rollins', '1998-05-01', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Ian', current_date, 'Smith', '1998-01-05', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Jayla', current_date, 'Williams', '1998-07-07', 1);
insert into Employee (FirstName, HireDate, LastName, DOB, RankID) values ('Kevin', current_date, 'Miller', '1998-07-09', 1);

/* Certification */
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 4);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 5);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 6);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 7);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 8);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 9);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 10);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 11);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 12);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 1, 13);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 2, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 2, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 2, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 2, 4);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 3, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 3, 2);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 4, 1);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 4);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 5);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 6);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 7);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 8);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 9);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 10);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 11);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 5, 12);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 4);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 4);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 8, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 8, 2);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 4);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 5);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 8);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 10);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 9, 12);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 1);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 2);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 4);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 5);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 8);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 10);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 10, 12);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 9);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 9);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 2, 9);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 6, 9);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 7, 9);

insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 3, 3);
insert into Certification (DateEarned, TrainingID, EmployeeID) values (current_date, 8, 3);

/* RankRequire */
insert into RankRequire (RankID, TrainingID) values (1, 1);
insert into RankRequire (RankID, TrainingID) values (1, 5);
insert into RankRequire (RankID, TrainingID) values (2, 2);
insert into RankRequire (RankID, TrainingID) values (2, 6);
insert into RankRequire (RankID, TrainingID) values (2, 7);
insert into RankRequire (RankID, TrainingID) values (3, 3);
insert into RankRequire (RankID, TrainingID) values (3, 8);
insert into RankRequire (RankID, TrainingID) values (4, 4);
insert into RankRequire (RankID, TrainingID) values (4, 9);
insert into RankRequire (RankID, TrainingID) values (4, 10);

/* Session */
insert into Session (SessionDate, TrainingID) values (current_date, 1);
insert into Session (SessionDate, TrainingID) values (current_date, 2);
insert into Session (SessionDate, TrainingID) values (current_date, 3);
insert into Session (SessionDate, TrainingID) values (current_date, 4);
insert into Session (SessionDate, TrainingID) values (current_date, 5);
insert into Session (SessionDate, TrainingID) values (current_date, 6);
insert into Session (SessionDate, TrainingID) values (current_date, 7);
insert into Session (SessionDate, TrainingID) values (current_date, 8);
insert into Session (SessionDate, TrainingID) values (current_date, 9);
insert into Session (SessionDate, TrainingID) values (current_date, 10);

/* Apparatus */
insert into Apparatus (LicensePlate, FireHouseID, VehicleTypeID) values ('ABC-4018', 1, 1);
insert into Apparatus (LicensePlate, FireHouseID, VehicleTypeID) values ('DEF-6022', 1, 1);
insert into Apparatus (LicensePlate, FireHouseID, VehicleTypeID) values ('GHI-3749', 1, 2);
insert into Apparatus (LicensePlate, FireHouseID, VehicleTypeID) values ('JKL-9945', 1, 3);

/* Shift */
insert into Shift (ShiftDate, CaptainID, FireHouseID) values ('2020-05-04', 1, 1);
insert into Shift (ShiftDate, CaptainID, FireHouseID) values ('2020-05-05', 2, 1);
insert into Shift (ShiftDate, CaptainID, FireHouseID) values ('2020-05-06', 1, 1);

/* ShiftLieutenant */
insert into ShiftLieutenant (LieutenantID, ShiftID) values (3, 1);
insert into ShiftLieutenant (LieutenantID, ShiftID) values (4, 1);
insert into ShiftLieutenant (LieutenantID, ShiftID) values (3, 2);
insert into ShiftLieutenant (LieutenantID, ShiftID) values (4, 2);
insert into ShiftLieutenant (LieutenantID, ShiftID) values (3, 2);
insert into ShiftLieutenant (LieutenantID, ShiftID) values (4, 2);

/* ShiftFirefighter */
insert into ShiftFirefighter (FirefighterID, ShiftID) values (5, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (6, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (7, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (8, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (9, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (10, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (11, 1);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (5, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (6, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (7, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (9, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (11, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (12, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (13, 2);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (7, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (8, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (9, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (10, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (11, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (12, 3);
insert into ShiftFirefighter (FirefighterID, ShiftID) values (13, 3);

/* Apparatus Captain */
insert into ApparatusCaptain (EmployeeID, ApparatusID) values (1, 1);
insert into ApparatusCaptain (EmployeeID, ApparatusID) values (2, 2);
insert into ApparatusCaptain (EmployeeID, ApparatusID) values (2, 3);

/* ApparatusFighter */
insert into ApparatusFighter (EmployeeID, ApparatusID) values (5, 1);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (6, 2);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (7, 3);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (9, 1);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (10, 2);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (11, 3);
insert into ApparatusFighter (EmployeeID, ApparatusID) values (8, 4);

/* Equipment */
insert into Equipment (ApparatusID, EquipmentName, NextService) values (1, 'Rope', '2020-05-09');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (1, 'Hose', '2020-07-25');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (2, 'Rope', '2020-05-15');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (2, 'Hose', '2020-07-11');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (3, 'Rope', '2020-05-10');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (3, 'Ladder', '2020-08-14');
insert into Equipment (ApparatusID, EquipmentName, NextService) values (4, 'Rope', '2020-05-13');

/* Attends */
insert into Attends (EmployeeID, SessionID) values (5, 1);
insert into Attends (EmployeeID, SessionID) values (6, 1);
insert into Attends (EmployeeID, SessionID) values (7, 1);
insert into Attends (EmployeeID, SessionID) values (8, 1);
insert into Attends (EmployeeID, SessionID) values (9, 1);
insert into Attends (EmployeeID, SessionID) values (10, 1);
insert into Attends (EmployeeID, SessionID) values (11, 1);
insert into Attends (EmployeeID, SessionID) values (12, 1);
insert into Attends (EmployeeID, SessionID) values (13, 1);
insert into Attends (EmployeeID, SessionID) values (3, 2);
insert into Attends (EmployeeID, SessionID) values (4, 2);
insert into Attends (EmployeeID, SessionID) values (2, 3);
insert into Attends (EmployeeID, SessionID) values (1, 4);
insert into Attends (EmployeeID, SessionID) values (1, 5);
insert into Attends (EmployeeID, SessionID) values (2, 5);
insert into Attends (EmployeeID, SessionID) values (3, 5);
insert into Attends (EmployeeID, SessionID) values (4, 5);
insert into Attends (EmployeeID, SessionID) values (5, 5);
insert into Attends (EmployeeID, SessionID) values (6, 5);
insert into Attends (EmployeeID, SessionID) values (7, 5);
insert into Attends (EmployeeID, SessionID) values (8, 5);
insert into Attends (EmployeeID, SessionID) values (9, 5);
insert into Attends (EmployeeID, SessionID) values (10, 5);
insert into Attends (EmployeeID, SessionID) values (11, 5);
insert into Attends (EmployeeID, SessionID) values (12, 5);
insert into Attends (EmployeeID, SessionID) values (13, 5);
insert into Attends (EmployeeID, SessionID) values (1, 6);
insert into Attends (EmployeeID, SessionID) values (2, 6);
insert into Attends (EmployeeID, SessionID) values (3, 6);
insert into Attends (EmployeeID, SessionID) values (4, 6);
insert into Attends (EmployeeID, SessionID) values (1, 7);
insert into Attends (EmployeeID, SessionID) values (2, 7);
insert into Attends (EmployeeID, SessionID) values (3, 7);
insert into Attends (EmployeeID, SessionID) values (4, 7);
insert into Attends (EmployeeID, SessionID) values (1, 8);
insert into Attends (EmployeeID, SessionID) values (2, 8);
insert into Attends (EmployeeID, SessionID) values (1, 9);
insert into Attends (EmployeeID, SessionID) values (2, 9);
insert into Attends (EmployeeID, SessionID) values (3, 9);
insert into Attends (EmployeeID, SessionID) values (4, 9);
insert into Attends (EmployeeID, SessionID) values (5, 9);
insert into Attends (EmployeeID, SessionID) values (8, 9);
insert into Attends (EmployeeID, SessionID) values (10, 9);
insert into Attends (EmployeeID, SessionID) values (12, 9);
insert into Attends (EmployeeID, SessionID) values (1, 10);
insert into Attends (EmployeeID, SessionID) values (2, 10);
insert into Attends (EmployeeID, SessionID) values (3, 10);
insert into Attends (EmployeeID, SessionID) values (4, 10);
insert into Attends (EmployeeID, SessionID) values (5, 10);
insert into Attends (EmployeeID, SessionID) values (8, 10);
insert into Attends (EmployeeID, SessionID) values (10, 10);
insert into Attends (EmployeeID, SessionID) values (12, 10);

/* Teaches */
insert into Teaches (SessionID, InstructorID) values (1, 1);
insert into Teaches (SessionID, InstructorID) values (2, 1);
insert into Teaches (SessionID, InstructorID) values (3, 1);
insert into Teaches (SessionID, InstructorID) values (4, 1);
insert into Teaches (SessionID, InstructorID) values (5, 1);
insert into Teaches (SessionID, InstructorID) values (6, 2);
insert into Teaches (SessionID, InstructorID) values (7, 2);
insert into Teaches (SessionID, InstructorID) values (8, 2);
insert into Teaches (SessionID, InstructorID) values (9, 2);
insert into Teaches (SessionID, InstructorID) values (10, 2);

/* EquipmentTraining */
insert into EquipmentTraining (EquipmentID, TrainingID) values (6, 5);
insert into EquipmentTraining (EquipmentID, TrainingID) values (2, 6);
insert into EquipmentTraining (EquipmentID, TrainingID) values (4, 6);
insert into EquipmentTraining (EquipmentID, TrainingID) values (1, 8);
insert into EquipmentTraining (EquipmentID, TrainingID) values (3, 8);
insert into EquipmentTraining (EquipmentID, TrainingID) values (5, 8);
insert into EquipmentTraining (EquipmentID, TrainingID) values (7, 8);



/*** INDEXING ***/

/* For Report A, Equipment should be indexed by the next service date to report 
   on required maintenence in order of due dates. 
*/
CREATE INDEX idx_equipment
ON Equipment (NextService);

/* For Reports B and C, Employee should be indexed by TrainingID, then
   EmployeeID. First, we index by TrainingID to make calculating the most and
   least received training easier, then by EmployeeID to make it
   easier to search if a particular employee has received a certain training. 
*/
CREATE INDEX idx_certification
ON Certification (TrainingID, EmployeeID);

/* For Report C, we index Rank by pkRank to make it easier to search for a
   particular rank. We index Training by pkTraining to make it easier to search
   for a particular training module (this depends on what trainings are 
   included in RankRequire). 
*/
CREATE INDEX idx_rank
ON Rank (pkRank);

CREATE INDEX idx_training
ON Training (pkTraining);

CREATE INDEX idx_rankrequire
ON RankRequire (TrainingID);

CREATE INDEX idx_employee
ON Employee (pkEmployee);

/* For Report D, we index ShiftLieutenant and ShiftFirefighter both by ShiftID.
   This makes it easier to count the number of Lieutenants and Firefighters, to
   determine if a proper number has been assigned to a particular shift.
*/
CREATE INDEX idx_shiftleiutenant
ON ShiftLieutenant (shiftID);

CREATE INDEX idx_shiftfirefighter
ON ShiftFirefighter (shiftID);