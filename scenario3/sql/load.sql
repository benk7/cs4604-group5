insert into Branch (Branch_id, manager_id) values (1, 1);
insert into Branch (Branch_id, manager_id) values (2, 2);

insert into Staff (Staff_id, Name, position, salary, Branch_id) values (1, 'Ben Kernstine', 'manager', 100000, 1);
insert into Staff (Staff_id, Name, position, salary, Branch_id) values (2, 'Sami Morency', 'manager', 100000, 2);
insert into Staff (Staff_id, Name, position, salary, Branch_id) values (3, 'Austin Spencer', 'sales', 95000, 1);

insert into Customer(Customer_id, Name, SSN) values (1, 'Thomas Super', 111002222);
insert into Customer(Customer_id, Name, SSN) values (2, 'Kyle Vincent', 123456789);

insert into Request(Request_id, cost, Customer_id) values (1, 1230, 1);

insert into House(House_id, address, status, Branch_id, Customer_id) values (1, '205 Tee St', 'sell', 1, 1);
insert into House(House_id, address, status, Branch_id, Customer_id) values (2, '9600 Hunters Mill Rd', 'rent', 2, 2);

insert into Contract(Contract_id, date, Customer_id, Branch_id) values (1, '2019-01-01', 1, 1);
insert into Contract(Contract_id, date, Customer_id, Branch_id) values (2, '2019-01-02', 2, 2);

insert into Extenstion(Extension_id, discount_value, Contract_id) values (1, 1000, 2);

insert into SellContract(SellContract_id, SellValue, Contract_id) values (1, 213000, 1);

insert into RentContract(RentContract_id, deposit, rent_cost, contract_id) values (1, 250, 1600, 2);

insert into LengthOfContracts(Length_id, length) values (1, 'six months');
insert into LengthOfContracts(Length_id, length) values (2, 'one year');
insert into LengthOfContracts(Length_id, length) values (3, 'two years');

insert into RentLengths(RentLength_id, RentContract_id, Length_id) values (1, 1, 1);
insert into RentLengths(RentLength_id, RentContract_id, Length_id) values (2, 1, 2);
insert into RentLengths(RentLength_id, RentContract_id, Length_id) values (3, 1, 3);