CREATE TABLE Patient
(
  pkPatient INT NOT NULL,
  Name VARCHAR NOT NULL,
  DOB DATE NOT NULL,
  Address VARCHAR NOT NULL,
  PRIMARY KEY (pkPatient)
);

CREATE TABLE Doctor
(
  pkDoctor INT NOT NULL,
  Name VARCHAR NOT NULL,
  Speciality VARCHAR NOT NULL,
  StartDate DATE NOT NULL,
  PRIMARY KEY (pkDoctor)
);

CREATE TABLE AdmissionRecord
(
  pkAdmission INT NOT NULL,
  AdmissionDate DATE NOT NULL,
  DischargeDate DATE NOT NULL,
  pkPatient INT NOT NULL,
  pkDoctor INT NOT NULL,
  PRIMARY KEY (pkAdmission),
  FOREIGN KEY (pkPatient) REFERENCES Patient(pkPatient),
  FOREIGN KEY (pkDoctor) REFERENCES Doctor(pkDoctor)
);

CREATE TABLE Ward
(
  pkWard INT NOT NULL,
  Name VARCHAR NOT NULL,
  Type VARCHAR NOT NULL,
  pkPatient INT NOT NULL,
  PRIMARY KEY (pkWard),
  FOREIGN KEY (pkPatient) REFERENCES Patient(pkPatient)
);

CREATE TABLE Bed
(
  pkBedNumber INT NOT NULL,
  Type VARCHAR NOT NULL,
  pkWard INT NOT NULL,
  PRIMARY KEY (pkBedNumber),
  FOREIGN KEY (pkWard) REFERENCES Ward(pkWard)
);

CREATE TABLE Operation
(
  pkOperation INT NOT NULL,
  OpDate DATE NOT NULL,
  OpTime TIME NOT NULL,
  pkPatient INT NOT NULL,
  PRIMARY KEY (pkOperation),
  FOREIGN KEY (pkPatient) REFERENCES Patient(pkPatient)
);

CREATE TABLE performs
(
  pkDoctor INT NOT NULL,
  pkOperation INT NOT NULL,
  PRIMARY KEY (pkDoctor, pkOperation),
  FOREIGN KEY (pkDoctor) REFERENCES Doctor(pkDoctor),
  FOREIGN KEY (pkOperation) REFERENCES Operation(pkOperation)
);
