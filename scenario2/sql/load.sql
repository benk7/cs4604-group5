insert into Gallery () values ();

insert into Artists (Name, Birthplace, Age, Style) values ('Vincent van Gogh', 'Zundert, Netherlands', 37, 'Post-impressionism');
insert into Artists (Name, Birthplace, Age, Style) values ('Claude Monet', 'Paris, France', 86, 'Impressionism');

insert into Customers (Name, Address, Dollars_Spent) values ('Austin Spencer', '14406 Birnam Woods Dr.', 55.49);
insert into Customers (Name, Address, Dollars_Spent) values ('Ben Kernstine', '1552 Main St.', 22.00);

insert into Artwork (Year, Title, Type, Price) values (1889, 'The Starry Night', 'Painting', 100000000.00);
insert into Artwork (Year, Title, Type, Price) values (1891, 'Haystacks', 'Painting', 110700000.00);

insert into ArtGroup (Name) values ('works by van Gogh');
insert into ArtGroup (Name) values ('works by Monet');
insert into ArtGroup (Name) values ('19th Century');

insert into GalleryArtists (ArtistName, Gallery_Id) values ('Vincent van Gogh', 1);
insert into GalleryArtists (ArtistName, Gallery_Id) values ('Claude Monet', 1);

insert into Visit (CustomerName, Gallery_Id) values ('Austin Spencer', 1);
insert into Visit (CustomerName, Gallery_Id) values ('Ben Kernstine', 1);

insert into LikedArtist (ArtistName, CustomerName) values ('Vincent van Gogh', 'Austin Spencer');
insert into LikedArtist (ArtistName, CustomerName) values ('Claude Monet', 'Austin Spencer');
insert into LikedArtist (ArtistName, CustomerName) values ('Claude Monet', 'Ben Kernstine');

insert into LikedArtGroup (CustomerName, GroupName) values ('Austin Spencer', 'works by van Gogh');
insert into LikedArtGroup (CustomerName, GroupName) values ('Austin Spencer', 'works by Monet');
insert into LikedArtGroup (CustomerName, GroupName) values ('Ben Kernstine', 'works by Monet');
insert into LikedArtGroup (CustomerName, GroupName) values ('Ben Kernstine', '19th Century');

insert into ArtworkInGroup (ArtworkTitle, GroupName) values ('The Starry Night', 'works by van Gogh');
insert into ArtworkInGroup (ArtworkTitle, GroupName) values ('The Starry Night', '19th Century');
insert into ArtworkInGroup (ArtworkTitle, GroupName) values ('Haystacks', 'works by Monet');
insert into ArtworkInGroup (ArtworkTitle, GroupName) values ('Haystacks', '19th Century');

insert into Owns (ArtworkTitle, ArtistName) values ('The Starry Night', 'Vincent van Gogh');
insert into Owns (ArtworkTitle, ArtistName) values ('Haystacks', 'Claude Monet');