CREATE SEQUENCE Gallery_Id
CREATE TABLE Gallery
(
  Id INT DEFAULT nextval('Gallery_Id') NOT NULL,
  PRIMARY KEY (Id)
);

CREATE TABLE Artists
(
  Name VARCHAR NOT NULL,
  Birthplace VARCHAR NOT NULL,
  Age INT NOT NULL,
  Style VARCHAR NOT NULL,
  PRIMARY KEY (Name),
  UNIQUE (Name)
);

CREATE TABLE Customers
(
  Name VARCHAR NOT NULL,
  Address VARCHAR NOT NULL,
  Dollars_Spent FLOAT NOT NULL,
  PRIMARY KEY (Name),
  UNIQUE (Name)
);

CREATE TABLE Artwork
(
  Year INT NOT NULL,
  Title VARCHAR NOT NULL,
  Type VARCHAR NOT NULL,
  Price FLOAT NOT NULL,
  PRIMARY KEY (Title),
  UNIQUE (Title)
);

CREATE TABLE ArtGroup
(
  Name VARCHAR NOT NULL,
  PRIMARY KEY (Name),
  UNIQUE (Name)
);

CREATE TABLE GalleryArtists
(
  ArtistName VARCHAR NOT NULL,
  Gallery_Id INT NOT NULL,
  PRIMARY KEY (ArtistName, Gallery_Id),
  FOREIGN KEY (ArtistName) REFERENCES Artists(Name),
  FOREIGN KEY (Gallery_Id) REFERENCES Gallery(Id)
);

CREATE TABLE Visit
(
  CustomerName VARCHAR NOT NULL,
  Gallery_Id INT NOT NULL,
  PRIMARY KEY (CustomerName, Gallery_Id),
  FOREIGN KEY (CustomerName) REFERENCES Customers(Name),
  FOREIGN KEY (Gallery_Id) REFERENCES Gallery(Id)
);

CREATE TABLE LikedArtist
(
  ArtistName VARCHAR NOT NULL,
  CustomerName VARCHAR NOT NULL,
  PRIMARY KEY (ArtistName, CustomerName),
  FOREIGN KEY (ArtistName) REFERENCES Artists(Name),
  FOREIGN KEY (CustomerName) REFERENCES Customers(Name)
);

CREATE TABLE LikedArtGroup
(
  CustomerName VARCHAR NOT NULL,
  GroupName VARCHAR NOT NULL,
  PRIMARY KEY (CustomerName, GroupName),
  FOREIGN KEY (CustomerName) REFERENCES ArtGroup(Name),
  FOREIGN KEY (GroupName) REFERENCES Customers(Name)
);

CREATE TABLE ArtworkInGroup
(
  ArtworkTitle VARCHAR NOT NULL,
  GroupName VARCHAR NOT NULL,
  PRIMARY KEY (ArtworkTitle, GroupName),
  FOREIGN KEY (ArtworkTitle) REFERENCES Artwork(Title),
  FOREIGN KEY (GroupName) REFERENCES ArtGroup(Name)
);

CREATE TABLE Owns
(
  ArtworkTitle VARCHAR NOT NULL,
  ArtistName VARCHAR NOT NULL,
  PRIMARY KEY (ArtworkTitle, ArtistName),
  FOREIGN KEY (ArtworkTitle) REFERENCES Artwork(Title),
  FOREIGN KEY (ArtistName) REFERENCES Artists(Name)
);