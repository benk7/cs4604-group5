USE scenario1;

INSERT INTO Patient (pkPatient, Name, DOB, Address) VALUES
(1, "Tommy Tables", '1988-01-05', "880 West Campus Dr, Blacksburg, VA 24061");

INSERT INTO Doctor (pkDoctor, Name, Speciality, StartDate) VALUES
(2, "Meredith Grey", "Surgeon", '2019-05-24');

INSERT INTO AdmissionRecord (pkAdmission, AdmissionDate, DischargeDate, pkPatient, pkDoctor) VALUES (3, '2020-02-07', '2020-02-09', 1, 2);

INSERT INTO Ward (pkWard, Name, Type, pkPatient) VALUES (4, "Intensive Care", "Surgical", 1);

INSERT INTO Bed (pkBedNumber, Type, pkWard) VALUES (5, "Double", 4);

INSERT INTO Operation (pkOperation, OpDate, OpTime, pkPatient) VALUES (5, '2020-03-01', '12:00:40', 1);

INSERT INTO Performs (pkDoctor, pkOperation) VALUES (2, 5);
