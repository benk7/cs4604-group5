# CS4604 Project - Scenario 2

## Project Description
Although you always wanted to be an artist, you ended up being an expert on databases because you love to cook data and you somehow confused “database” with “data baste”. Your old love is still there, however, so you set up a database company, ArtBase that builds a product for art galleries. The core of this product is a database with a schema that captures all the information that galleries need to maintain. Galleries keep information about artists, their names (which are unique), birthplaces, age, and style of art. For each piece of artwork, the artist, the year it was made, its unique title, its type of art (e.g., painting, lithograph, sculpture, photograph), and its price must be stored. Pieces of artwork are also classified into groups of various kinds, for example, portraits, still-lifes, works by Picasso, or works of the 19th century; a given piece may belong to more than one group. Each group is identified by a name (like those just given) that describes the group. Finally, galleries keep information about customers. For each customer, galleries keep that person’s unique name, address, total amount of dollars spent in gallery (very important!), and the artists and groups of art that the customer tends to like.

## Entity Relationship

The entities in this project are a *Galleries*, *Artists*, *Artwork*, *ArtGroups*, and *Customers*.

A Gallery is uniquely identified by *Id*.
Artists, ArtGroups, and Customers are uniquely identified by *Name*.
Artwork is uniquely identified by *Title*.

Galleries contain many Artists and Customers.
Artists can belong to any amount of Galleries and own any amount of Artworks.
Artworks belong to one or more artists and belong to any amount of ArtGroups.
ArtGroups contain any amount of Artworks.
Customers like any amount of Artists and ArtGroups.

![ER Diagram](diagrams/entity-relationship.png  "ER Diagram")

## Relational

The diagram below shows the entity-relationship diagram converted to a relational schema.

![Relational Diagram](diagrams/relational.png "Relational Diagram")

The corresponding sql can be found [here](sql/install.sql).

## Build/Install/Run

### Docker Playground

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

#### Start Postgresql server

`docker run -d --name postgres -e POSTGRES_HOST_AUTH_METHOD=trust postgres -c 'log_min_duration_statement=0'`

#### Create the schema

```
curl https://code.vt.edu/benk7/cs4604-group5/tree/master/scenario2/sql/install.sql > install.sql
docker run -it --rm -v $(pwd)/install.sql:/install.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /install.sql
```

#### Load data

```
curl https://code.vt.edu/benk7/cs4604-group5/tree/master/scenario2/sql/load.sql > load.sql
docker run -it --rm -v $(pwd)/load.sql:/load.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /load.sql
```

#### Browse the database

`docker run -it --rm --link postgres:postgres postgres psql -a -h postgres -U postgres`

Use `\d` to see the list of available relations.
