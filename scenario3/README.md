# CS4604 Project - Scenario 3

## Project Description

We are asked to design a database management system for all information related to a real-estate company which has several branches throughout the United States. The first step is to organize the information given about the company. We have collected the following data:

* Each branch has a unique branch number. It allocated staff, which includes one Manager. Also, each branch has a list of available houses to rent/sell.
* The manager is responsible for the day-to-day running of a given branch.
* Each staff has a unique staff number, name, position, salary, and branch number.
* Each house has a unique house number, address, rent cost, sell value, status, and branch number.
* The status of a house indicates whether it is available for rent/sell. The branch number indicate which branch of the real-estate company can rent/sell the house.
* A Customer has SSN, name, contract number, house number.
* Each Contract has a unique contract number, type, date, branch number, and customer number.
* If the type of a contract is rent, it will have a deposit, and rent cost, and lengths of contract. The length of a rental contract can be six months, one year or 2 years, but customers can extend their contract by signing a new contract. 
* Each contract extension has a unique extension number, the original contract number, and discount value.
* If the contract type is "sell", it will have sell value.
* Customers can issue a contract termination request. Each termination request has a unique request number, customer number, and termination cost.


## Entity Relationship
![ER Diagram](diagrams/scenario3-erd.png  "ER Diagram")

## Relational

The diagram below shows the entity-relationship diagram converted to a relational schema.

![Relational Diagram](diagrams/scenario3-relational.png "Relational Diagram")

The corresponding sql can be found [here](sql/install.sql).

## Build/Install/Run

### Docker Playground

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

#### Start Postgresql server

`docker run -d --name postgres -e POSTGRES_HOST_AUTH_METHOD=trust postgres -c 'log_min_duration_statement=0'`

#### Create the schema

```
curl https://code.vt.edu/benk7/cs4604-group5/tree/master/scenario3/sql/install.sql > install.sql
docker run -it --rm -v $(pwd)/install.sql:/install.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /install.sql
```

#### Load data

```
curl https://code.vt.edu/benk7/cs4604-group5/tree/master/scenario3/sql/load.sql > load.sql
docker run -it --rm -v $(pwd)/load.sql:/load.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /load.sql
```

#### Browse the database

`docker run -it --rm --link postgres:postgres postgres psql -a -h postgres -U postgres`

Use `\d` to see the list of available relations.
