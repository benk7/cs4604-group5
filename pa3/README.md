# CS4604 Project - Part 3

## Project Description

The Chief of the Hokie Fire Department has asked your team to design an ERP for them. He needs to keep track of employees, equipment and schedules. In addition to regular employee information such as name, employee number, hire date, date of birth, etc. firefighters also have a rank, trainings completed and certifications. Each position within the fire department requires firefighters to have one or more certifications. Anyone having the required certifications is qualified for that position (though not necessarily in the position). Positions, also called ranks, are Firefighter, Lieutenant, Captain and Chief.

Training consists of one or more sessions and are taught by one or more qualified instructors. Some trainings allow the student to receive a certification. Some certifications require an exam in addition to the training.

Each firefighter is a member of a "shift". A shift consists of 7-9 Firefighters, 2 Lieutenants and a Captain. All firefighters must be certified as a "Firefighter 1" (or higher) and in "Vehicle Rescue". All Lieutenants must have certifications for "Emergency Vehicle Operator" and "Pump Operator". Captains must have the "Fire Investigator" certification. Each shift must also have at least one person certified as a "Paramedic" and at least one person certified in "Hazardous Materials".

Each shift is assigned to a fire house and works a 24 hours on, 48 hours off rotation. That is, the shift is on duty for 24 hours and then off duty for 48 hours. An individual employee may swap duty with another employee for a given day provided that the shift's certification requirements are met.

A fire apparatus is either an "engine", "ladder truck" or "utility vehicle." Ladder trucks and engines must be crewed by at least one officer (Lieutenant or Captain) and two or more firefighters. A utility vehicle is manned by one firefighter. A shift can man two fire engines, a ladder truck and one utility truck.

Each piece of equipment (ladders, hose, etc.) is assigned to a particular fire apparatus. Each apparatus is assigned to a fire house. Equipment must have periodic maintenance performed based on a schedule for that type of equipment. For example, all ladders must be cleaned and inspected monthly and have their rope replaced annually. There is specific training for each piece of equipment.

In these diagrams there we're several things we could not model. We couldn't model the 24 hours on 48 hours off of a shift and what types of employees are on a shift. The former is handled by giving the date time of the shift and the latter by an intersection table of *Shift* and *Employee*.  

## Entity Relationship

The entities in this project are a *Employee*, *Rank*, *Shift*, *FireHouse*, *Apparatus*, *Equipment*, *VehicleType*, *Training*, *Certification*, *Session*, *RankRequire*, and an *Instructor*. An Employee *has a* a Rank, *works a* Shift, *attends a* Session, and *earns a* Certification. A Certification *requires a* Training, an Instructor *teaches* a Session, and a Session *is the subject of* a Training. In order to reach a certain Rank you must own all the Training IDs associated with that rank.  A Shift *takes place at* a FireHouse, which *has an* Apparatus. An Apparatus *is assigned to* Equipment and *has a* VehicleType.

Each Entity is uniquely defined by pkNameOfEntity. All foreign keys are designated with NameOfForeignTableID.

![ER Diagram](diagrams/ERD.png "ER Diagram")

## Relational

The diagram below shows the entity-relationship diagram converted to a relational schema.

![Relational Diagram](diagrams/relational.png "Relational Diagram")

The corresponding sql can be found [here](sql/install.sql).

## Build/Install/Run

### Docker Playground

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user.

#### Start Postgresql server

`docker run -d --name postgres -e POSTGRES_HOST_AUTH_METHOD=trust postgres -c 'log_min_duration_statement=0'`

#### Create the schema

```
curl -o install.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/install.sql
docker run -it --rm -v $(pwd)/install.sql:/install.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /install.sql
```

#### Load data

```
curl -o load.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/load.sql
docker run -it --rm -v $(pwd)/load.sql:/load.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /load.sql
```
#### Load reporting SQL files

##### maintenance

```
curl -o report-maintenance.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/report-maintenance.sql
docker run -it --rm -v $(pwd)/report-maintenance.sql:/report-maintenance.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /report-maintenance.sql
```

##### training

```
curl -o report-training.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/report-training.sql
docker run -it --rm -v $(pwd)/report-training.sql:/report-training.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /report-training.sql
```

##### promote

```
curl -o report-promote.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/report-promote.sql
docker run -it --rm -v $(pwd)/report-promote.sql:/report-promote.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /report-promote.sql
```

##### understaffed

```
curl -o report-understaffed.sql https://code.vt.edu/benk7/cs4604-group5/raw/master/pa3/sql/report-understaffed.sql
docker run -it --rm -v $(pwd)/report-understaffed.sql:/report-understaffed.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /report-understaffed.sql
```

#### Browse the database

`docker run -it --rm --link postgres:postgres postgres psql -a -h postgres -U postgres`

Use `\d` to see the list of available relations.
