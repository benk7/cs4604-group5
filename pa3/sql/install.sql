CREATE TABLE Rank
(
  pkRank SERIAL PRIMARY KEY,
  RankName VARCHAR(300) NOT NULL
);

CREATE TABLE FireHouse
(
  pkFireHouse SERIAL PRIMARY KEY,
  Line1 VARCHAR(300) NOT NULL,
  Line2 VARCHAR(300) NOT NULL,
  City VARCHAR(300) NOT NULL,
  State VARCHAR(300) NOT NULL,
  Zip INT NOT NULL
);

CREATE TABLE Training
(
  pkTraining SERIAL PRIMARY KEY,
  TrainingName VARCHAR(300) NOT NULL,
  ExamNeeded INT NOT NULL
);

CREATE TABLE Session
(
  pkSession SERIAL PRIMARY KEY,
  SessionDate DATE NOT NULL,
  TrainingID INT NOT NULL,
  FOREIGN KEY (TrainingID) REFERENCES Training(pkTraining)
);

CREATE TABLE Instructor
(
  pkInstructor SERIAL PRIMARY KEY,
  Name VARCHAR(300) NOT NULL
);

CREATE TABLE RankRequire
(
  pkRankRequire SERIAL PRIMARY KEY,
  RankID INT NOT NULL,
  TrainingID INT NOT NULL,
  FOREIGN KEY (RankID) REFERENCES Rank(pkRank),
  FOREIGN KEY (TrainingID) REFERENCES Training(pkTraining)
);

CREATE TABLE teaches
(
  pkTeaches SERIAL PRIMARY KEY,
  InstructorID INT NOT NULL,
  SessionID INT NOT NULL,
  FOREIGN KEY (InstructorID) REFERENCES Instructor(pkInstructor),
  FOREIGN KEY (SessionID) REFERENCES Session(pkSession)
);

CREATE TABLE VehicleType
(
  pkVehicleType SERIAL PRIMARY KEY,
  VehicleTypeName VARCHAR(300) NOT NULL
);

CREATE TABLE Employee
(
  FirstName VARCHAR(300) NOT NULL,
  pkEmployee SERIAL PRIMARY KEY,
  HireDate DATE NOT NULL,
  LastName VARCHAR(300) NOT NULL,
  DOB DATE NOT NULL,
  RankID INT NOT NULL,
  FOREIGN KEY (RankID) REFERENCES Rank(pkRank)
);

CREATE TABLE Apparatus
(
  pkApparatus SERIAL PRIMARY KEY,
  LicensePlate VARCHAR(300) NOT NULL,
  FireHouseID INT NOT NULL,
  VehicleTypeID INT NOT NULL,
  FOREIGN KEY (FireHouseID) REFERENCES FireHouse(pkFireHouse),
  FOREIGN KEY (VehicleTypeID) REFERENCES VehicleType(pkVehicleType)
);

CREATE TABLE Shift
(
  pkShift SERIAL PRIMARY KEY,
  ShiftDate DATE NOT NULL,
  FireHouseID INT NOT NULL,
  CaptainID INT NOT NULL,
  FOREIGN KEY (FireHouseID) REFERENCES FireHouse(pkFireHouse),
  FOREIGN KEY (CaptainID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE Certification
(
  pkCertification SERIAL PRIMARY KEY,
  DateEarned Date NULL,
  TrainingID INT NOT NULL,
  EmployeeID INT NOT NULL,
  FOREIGN KEY (TrainingID) REFERENCES Training(pkTraining),
  FOREIGN KEY (EmployeeID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE attends
(
  pkAttends SERIAL PRIMARY KEY,
  EmployeeID INT NOT NULL,
  SessionID INT NOT NULL,
  FOREIGN KEY (EmployeeID) REFERENCES Employee(pkEmployee),
  FOREIGN KEY (SessionID) REFERENCES Session(pkSession)
);

CREATE TABLE ShiftLieutenant
(
  pkShiftLieutenant SERIAL PRIMARY KEY,
  ShiftID INT NOT NULL,
  LieutenantID INT NOT NULL,
  FOREIGN KEY (ShiftID) REFERENCES Shift(pkShift),
  FOREIGN KEY (LieutenantID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE ShiftFirefighter
(
  pkShiftFirefighter SERIAL PRIMARY KEY,
  ShiftID INT NOT NULL,
  FirefighterID INT NOT NULL,
  FOREIGN KEY (ShiftID) REFERENCES Shift(pkShift),
  FOREIGN KEY (FirefighterID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE ApparatusCaptain
(
  pkApparatusCaptain SERIAL PRIMARY KEY,
  ApparatusID INT NOT NULL,
  EmployeeID INT NOT NULL,
  FOREIGN KEY (ApparatusID) REFERENCES Apparatus(pkApparatus),
  FOREIGN KEY (EmployeeID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE ApparatusFighter
(
  pkApparatusFighter SERIAL PRIMARY KEY,
  ApparatusID INT NOT NULL,
  EmployeeID INT NOT NULL,
  FOREIGN KEY (ApparatusID) REFERENCES Apparatus(pkApparatus),
  FOREIGN KEY (EmployeeID) REFERENCES Employee(pkEmployee)
);

CREATE TABLE Equipment
(
  pkEquipment SERIAL PRIMARY KEY,
  EquipmentName VARCHAR(300) NOT NULL,
  NextService DATE NOT NULL,
  ApparatusID INT NOT NULL,
  FOREIGN KEY (ApparatusID) REFERENCES Apparatus(pkApparatus)
);

CREATE TABLE EquipmentTraining
(
  pkEquipmentTraining SERIAL PRIMARY KEY,
  TrainingID INT NOT NULL,
  EquipmentID INT NOT NULL,
  FOREIGN KEY (TrainingID) REFERENCES Training(pkTraining),
  FOREIGN KEY (EquipmentID) REFERENCES Equipment(pkEquipment)
);