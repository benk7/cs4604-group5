CREATE SEQUENCE Branch_Id
CREATE TABLE Branch
(
  Branch_id INT DEFAULT nextval('Branch_Id') NOT NULL,
  FOREIGN KEY (manager_id) REFERENCES Staff(Staff_id),
  PRIMARY KEY (Branch_id)
);

CREATE TABLE Staff
(
  Staff_id INT DEFAULT nextval('Staff_id') NOT NULL,
  Name VARCHAR NOT NULL,
  position VARCHAR NOT NULL,
  salary INT NOT NULL,
  FOREIGN KEY (branch_id) REFERENCES Branch(Branch_id),
  PRIMARY KEY (Staff_id)
);

CREATE TABLE Customer
(
  Customer_id INT DEFAULT nextval('Customer_id') NOT NULL,
  Name VARCHAR NOT NULL,
  SSN VARCHAR NOT NULL,
  PRIMARY KEY (Customer_id)
);

CREATE TABLE Request
(
  Request_id INT DEFAULT nextval('Request_id') NOT NULL,
  cost INT NOT NULL,
  FOREIGN KEY (Customer_id) REFERENCES Customer(Customer_id),
  PRIMARY KEY (Request_id)
);

CREATE TABLE House
(
  House_id INT DEFAULT nextval('House_id') NOT NULL,
  address VARCHAR NOT NULL,
  status VARCHAR NOT NULL,
  FOREIGN KEY (Branch_id) REFERENCES Branch(Branch_id),
  FOREIGN KEY (Customer_id) REFERENCES Customer(Customer_id),
  UNIQUE(address),
  PRIMARY KEY (House_id)
);

CREATE TABLE Contract
(
  Contract_id INT DEFAULT nextval('Contract_id') NOT NULL,
  date DATE NOT NULL,
  FOREIGN KEY (Customer_id) REFERENCES Customer(customer_id),
  FOREIGN KEY (Branch_id) REFERENCES Branch(Branch_id),
  PRIMARY KEY (Contract_id)
);

CREATE TABLE Extenstion
(
  Extenstion_id INT DEFAULT nextval('Extension_id') NOT NULL,
  discount_value INT NOT NULL,
  FOREIGN KEY (Contract_id) REFERENCES Contract(Contract_id),
  PRIMARY KEY (Extension_id)
);

CREATE TABLE SellContract
(
  SellContract_id INT DEFAULT nextval('SellContract_id') NOT NULL,
  SellValue INT NOT NULL,
  PRIMARY KEY (SellContract_id),
  FOREIGN KEY (Contract_id) REFERENCES Contract(Contract_id)
);

CREATE TABLE RentContract
(
  RentContract_id INT DEFAULT nextval('RentContract_id') NOT NULL,
  deposit INT NOT NULL,
  rent_cost INT NOT NULL,
  PRIMARY KEY (RentContract_id),
  FOREIGN KEY (Contract_id) REFERENCES Contract(Contract_id)
);

CREATE TABLE LengthOfContracts
(
  Length_id INT DEFAULT nextval('Length_id') NOT NULL,
  Length VARCHAR NOT NULL,
  PRIMARY KEY (Length_id)
);

CREATE TABLE RentLengths
(
  RentLength_id INT DEFAULT nextval('RentLength_id') NOT NULL,
  PRIMARY KEY (RentLength),
  FOREIGN KEY (RentContract_id) REFERENCES RentContract(RentContract_id),
  FOREIGN KEY (Length_id) REFERENCES LengthOfContracts(Length_id)
);
