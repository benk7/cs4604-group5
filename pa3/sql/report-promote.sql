SELECT e.FirstName, e.LastName FROM Employee e
INNER JOIN Rank r ON e.rankID = r.pkRank
WHERE r.RankName = 'Firefighter' AND 
    (SELECT COUNT(*) FROM RankREquire rr  
    INNER JOIN Rank r ON rr.RankID = r.pkRank
    WHERE r.RankName = 'Lieutenant') = 
        (SELECT COUNT(*) FROM Certification c
        INNER JOIN Training t on c.TrainingID = t.pkTraining
        INNER JOIN RankRequire rr ON t.pkTraining = rr.TrainingID
        INNER JOIN Employee e2 ON c.EmployeeID = e2.pkEmployee
        WHERE e2.pkEmployee = e.pkEmployee AND c.TrainingID IN 
            (SELECT rr.trainingID FROM RankREquire rr  
            INNER JOIN Rank r ON rr.RankID = r.pkRank
            WHERE r.RankName = 'Lieutenant'));
            
UNION 

SELECT e.FirstName, e.LastName FROM Employee e
INNER JOIN Rank r ON e.rankID = r.pkRank
WHERE r.RankName = 'Lieutenant' AND 
    (SELECT COUNT(*) FROM RankREquire rr  
    INNER JOIN Rank r ON rr.RankID = r.pkRank
    WHERE r.RankName = 'Captain') = 
        (SELECT COUNT(*) FROM Certification c
        INNER JOIN Training t on c.TrainingID = t.pkTraining
        INNER JOIN RankRequire rr ON t.pkTraining = rr.TrainingID
        INNER JOIN Employee e2 ON c.EmployeeID = e2.pkEmployee
        WHERE e2.pkEmployee = e.pkEmployee AND c.TrainingID IN 
            (SELECT rr.trainingID FROM RankREquire rr  
            INNER JOIN Rank r ON rr.RankID = r.pkRank
            WHERE r.RankName = 'Captain'));
            
UNION

SELECT e.FirstName, e.LastName FROM Employee e
INNER JOIN Rank r ON e.rankID = r.pkRank
WHERE r.RankName = 'Captain' AND 
    (SELECT COUNT(*) FROM RankREquire rr  
    INNER JOIN Rank r ON rr.RankID = r.pkRank
    WHERE r.RankName = 'Chief') = 
        (SELECT COUNT(*) FROM Certification c
        INNER JOIN Training t on c.TrainingID = t.pkTraining
        INNER JOIN RankRequire rr ON t.pkTraining = rr.TrainingID
        INNER JOIN Employee e2 ON c.EmployeeID = e2.pkEmployee
        WHERE e2.pkEmployee = e.pkEmployee AND c.TrainingID IN 
            (SELECT rr.trainingID FROM RankREquire rr  
            INNER JOIN Rank r ON rr.RankID = r.pkRank
            WHERE r.RankName = 'Chief'));


